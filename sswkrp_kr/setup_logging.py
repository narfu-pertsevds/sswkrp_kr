import atexit
import logging
import logging.handlers
import multiprocessing


def setup_logging():
    stderr = logging.StreamHandler()
    queue = multiprocessing.Queue()
    queue_handler = logging.handlers.QueueHandler(queue)
    queue_listener = logging.handlers.QueueListener(queue, stderr)
    queue_listener.start()
    atexit.register(queue_listener.stop)
    logging.basicConfig(level=logging.INFO, handlers=[queue_handler])
