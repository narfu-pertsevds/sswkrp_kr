import asyncio
import functools
from concurrent.futures import ProcessPoolExecutor
from dataclasses import dataclass
from typing import List

import aiofiles
import jsons

from .logger import logger


@dataclass
class Measurement(jsons.JsonSerializable):
    station_name: str
    measurement: float


async def jsons_dumps_async(object):
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(None, jsons.dumps, object)


async def jsons_loads_async(lines, object):
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(None, jsons.loads, lines, object)


async def load_from_file(filename):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        return await f.read()


async def get_max(loop, measurements):
    return await loop.run_in_executor(
        None,
        functools.partial(max, measurements, key=lambda item: float(item.measurement)),
    )


async def get_min(loop, measurements):
    return await loop.run_in_executor(
        None,
        functools.partial(min, measurements, key=lambda item: float(item.measurement)),
    )


async def get_max2(loop, measurements):
    return await loop.run_in_executor(
        None,
        functools.partial(
            max, measurements, key=lambda item: float(item[1].measurement)
        ),
    )


async def get_min2(loop, measurements):
    return await loop.run_in_executor(
        None,
        functools.partial(
            min, measurements, key=lambda item: float(item[0].measurement)
        ),
    )


async def run(n):
    loop = asyncio.get_event_loop()
    lines = await load_from_file(f"my{n}.json")
    loaded = await jsons_loads_async(lines, List[Measurement])
    return await asyncio.gather(get_min(loop, loaded), get_max(loop, loaded))


def run_proc(n):
    return asyncio.run(run(n))


async def app():
    cpus = 8
    loop = asyncio.get_event_loop()
    with ProcessPoolExecutor(max_workers=cpus) as executor:
        tasks = [loop.run_in_executor(executor, run_proc, i) for i in range(cpus)]
        res = await asyncio.gather(*tasks)
        max1 = await get_max2(loop, res)
        min1 = await get_min2(loop, res)
        logger.info(f"Максимум: {max1[1]}")
        logger.info(f"Минимум: {min1[0]}")
