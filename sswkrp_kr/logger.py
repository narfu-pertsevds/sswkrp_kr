import logging

# Module logger
module = __name__[::-1].split(".",1)[-1][::-1]
# Имя логгера = имя модуля
logger = logging.getLogger(module)
logger.addHandler(logging.NullHandler())
