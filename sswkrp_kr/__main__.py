import asyncio
import sys
from .app import app
from .setup_logging import setup_logging


def main():
    setup_logging()
    res = asyncio.run(app())
    sys.exit(res)

main()
